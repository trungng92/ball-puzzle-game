goog.provide('BG');

goog.require('BG.Grid');
goog.require('BG.Tile');
goog.require('BG.Turret');

var grid;
BG = {};

/**
 *
 */
BG.main = function() {
    BG.startGame();
};

/**
 * @return {void}
 */
BG.startGame = function() {
    Crafty.init(800, 600, document.getElementById('game'));

    var callback = function() {
        BG.registerComponents();
        grid = new BG.Grid(16, 12, 32);
    };

    BG.loadAssets(callback);
};

/**
 * @return {void}
 * @param {Function} callback
 */
BG.loadAssets = function(callback) {
    var assets = {
        'sprites': {
            'assets/ball.png': {
                'tile': 16,
                'tileh': 16,
                'map': {'BallSprite': [0, 0]}
            },
            'assets/tile.png': {
                'tile': 32,
                'tileh': 32,
                'map': {'TileSprite': [0, 0]}
            },
            'assets/turret_base.png': {
                'tile': 32,
                'tileh': 32,
                'map': {'TurretBaseSprite': [0, 0]}
            },
            'assets/turret_gun.png': {
                'tile': 32,
                'tileh': 32,
                'map': {'TurretGunSprite': [0, 0]}
            }
        }
    };
    Crafty.load(assets, callback);
};

/**
 * @return {void}
 */
BG.registerComponents = function() {
    BG.Ball.registerComponent();
    BG.Turret.registerComponent();
    BG.Tile.registerComponent();
};

/**
 * @return {void}
 */
BG.newBlock = function() {
    Crafty('BGTurret').each(function() {
        this.forceShoot();
    });
};

goog.exportSymbol('BG.main', BG.main);

assert = function(condition, text) {
    if (!condition) {
        debugger;
        console.log(text);
    }
};
