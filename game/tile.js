goog.provide('BG.Tile');

/**
 *
 * @constructor
 */
BG.Tile = function() {
    /** @type {Crafty.entity} */
    this.turret = null;
};

/**
 * @return {void}
 */
BG.Tile.registerComponent = function() {
    Crafty.c('BGTile', new BG.Tile());
};

/**
 *
 * @return {Crafty.entity}
 * @param {number} x
 * @param {number} y
 * @param {number} size
 */
BG.Tile.createEntity = function(x, y, size) {
    BG.Tile.createEntity.tileCounter++;
    return Crafty.e('BGTile, 2D, Canvas, Mouse, TileSprite')
        .attr({x: x,
            y: y,
            w: size,
            h: size})
        .bind('Click', function() {
            if (!this.hasTurret()) {
                var turret = BG.Turret.createEntity(this);
                var test = $("#guns paper-checkbox[label='Top']").attr('checked');
                if($("#guns paper-checkbox[label='Top']").attr('checked')) {
                    turret.setDirection('north', true);
                }
                if($("#guns paper-checkbox[label='Right']").attr('checked')) {
                    turret.setDirection('east', true);
                }
                if($("#guns paper-checkbox[label='Left']").attr('checked')) {
                    turret.setDirection('west', true);
                }
                if($("#guns paper-checkbox[label='Bottom']").attr('checked')) {
                    turret.setDirection('south', true);
                }
                this.setTurret(turret);
            } else {
                this.turret.rotateTurretCW();
                this.turret.forceShoot();
            }
       })
        .setName('Tile ' + BG.Tile.createEntity.tileCounter);
};
/** @type {number} */
BG.Tile.createEntity.tileCounter = 0;

/**
 * @return {boolean}
 */
BG.Tile.prototype.hasTurret = function() {
    // cast to boolean
    return !!this.turret;
};

/**
 *
 * @param {Crafty.entity} turret
 */
BG.Tile.prototype.setTurret = function(turret) {
    this.turret = turret;
};
