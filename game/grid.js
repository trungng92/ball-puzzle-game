goog.provide('BG.Grid');

goog.require('BG.Tile');


/**
 * TODO: make grid an entity and add all of the tiles as children
 * @param {number} numColumns
 * @param {number} numRows
 * @param {number} tileSize
 * @constructor
 */
BG.Grid = function(numColumns, numRows, tileSize) {
    /** @type {Array.<BG.Tile>} */
    this.tiles = [];
    /** @type {number} */
    this.tileSize = tileSize;
    /** @type {number} */
    this.numColumns = numColumns;
    /** @type {number} */
    this.numRows = numRows;

    for (var i = 0; i < numRows; i++)
    {
        for (var j = 0; j < numColumns; j++)
        {
            x = j * tileSize;
            y = i * tileSize;
            var tile = BG.Tile.createEntity(x, y, tileSize);
            this.tiles.push(tile);
        }
    }
};

/**
 *
 * @param {number} column
 * @param {number} row
 * @return {BG.Tile}
 */
BG.Grid.prototype.getTile = function(column, row) {
    return this.tiles[row * this.numColumns + column];
};

/**
 *
 * @param {number} column
 * @param {number} row
 * @param {BG.Tile} tile
 */
BG.Grid.prototype.setTile = function(column, row, tile) {
    this.tiles[row * this.numColumns + column] = tile;
};
