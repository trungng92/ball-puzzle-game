goog.provide('BG.Turret');

goog.require('BG.Ball');

/**
 *
 * @constructor
 */
BG.Turret = function() {
    /** @type {Object.<BG.Turret.Direction>} */
    this.direction = null;

    /** @type {boolean} */
    this.markedToShoot = false;
};
/** @type {number} */
BG.Turret.ROTATION_AMOUNT = 90;
/** @type {number} */
BG.Turret.FULL_ROTATION = 360;

/**
 * @return {void}
 */
BG.Turret.prototype.init = function() {
    this.direction = {};
    this.direction.north = new BG.Turret.Direction();
    this.direction.south = new BG.Turret.Direction();
    this.direction.east = new BG.Turret.Direction();
    this.direction.west = new BG.Turret.Direction();
};

/**
 * @return {void}
 */
BG.Turret.registerComponent = function() {
    Crafty.c('BGTurret', new BG.Turret());
};

/**
 * @return {Crafty.entity}
 * @param {Crafty.entity} tile
 */
BG.Turret.createEntity = function(tile) {
    BG.Turret.createEntity.tileCounter++;
    var that = Crafty.e('TileObject, BGTurret, 2D, Canvas, Collision');

        that.attr({x: tile.x,
              y: tile.y,
              w: tile.w,
              h: tile.h});
        that.onHit('BGBall', that.onHitBall)
        .setName('Turret ' + BG.Turret.createEntity.tileCounter);
    var turret_base = Crafty.e('Canvas, TurretBaseSprite')
        .attr({x: tile.x,
            y: tile.y,
            w: tile.w,
            h: tile.h});
    that.attach(turret_base);
    tile.attach(that);
    return that;
};
/** @type {number} */
BG.Turret.createEntity.tileCounter = 0;

/**
 * @return {void}
 * @param {Array.<Crafty.entity>} balls
 */
BG.Turret.prototype.onHitBall = function(balls) {
    for (var i = 0; i < balls.length; i++) {
        var ball = balls[i].obj;
        if (this !== ball.spawnedFrom) {
            var direction_side = ball.getOppositeDirection();
            if (!this.markedToShoot &&
                this.direction[direction_side].hasGun()) {
                this.markToShoot();
            }
            // get the opposite direction because a ball
            // that is going south will hit the turrets north
            if (this.direction[direction_side]) {
                this.direction[direction_side].disable();
                var that = this;
                this.uniqueBind('ExitFrame', that.onBallCollision);
            }
            ball.markForRemoval();
        }
    }
};

/**
 *
 * @return {void}
 * @param {string} direction
 */
BG.Turret.prototype.addGun = function(direction) {
    if (!this.direction[direction].hasGun()) {
        var e = Crafty.e('Canvas, TurretGunSprite')
            .origin('center')
            .attr({x: this.x,
                y: this.y,
                w: this.w,
                h: this.h});
        switch (direction) {
            case 'north':
                break;
            case 'east':
                e.rotation = BG.Turret.ROTATION_AMOUNT;
                break;
            case 'south':
                e.rotation = BG.Turret.ROTATION_AMOUNT * 2;
                break;
            case 'west':
                e.rotation = BG.Turret.ROTATION_AMOUNT * 3;
                break;
            default:
                assert(false, 'invalid direction');
        }
        this.attach(e);
        // possibly TODO: direction may need a reference to the gun
        // for rotations
        this.direction[direction].setGun(e);
        this.direction[direction].setHasGun(true);
    }
};

/**
 *
 * @return {void}
 * @param {string} direction
 */
BG.Turret.prototype.removeGun = function(direction) {
    if (this.direction[direction].hasGun()) {
        var e = this.direction[direction].getGun();
        e.destroy();
        this.direction[direction].setGun(null);
        this.direction[direction].setHasGun(false);
    }
};

/**
 *
 * @return {void}
 * @param {string} direction
 * @param {boolean} bool
 */
BG.Turret.prototype.setDirection = function(direction, bool) {
    bool ? this.addGun(direction) : this.removeGun(direction);
};

/**
 *
 * @return {void}
 */
BG.Turret.prototype.markToShoot = function() {
    for (var i in this.direction) {
        this.direction[i].enable();
    }
    this.markedToShoot = true;
};

/**
 * callback when a ball is hit
 * @return {void}
 */
BG.Turret.prototype.onBallCollision = function() {
    Crafty.trigger('RemoveBalls', null);
    this.shoot();
    this.unbind('ExitFrame', this.onBallCollision);
};

/**
 * @return {void}
 */
BG.Turret.prototype.forceShoot = function() {
    this.enableAllSides();
    this.shoot();
};

/**
 * @return {void}
 */
BG.Turret.prototype.shoot = function() {
    var speed = 1;
    var pos = BG.Turret.prototype.shoot.tmp_vec2;
    if (this.direction.south.canShoot()) {
        pos.x = this.x + this.w / 2;
        pos.y = this.y + this.h;
        BG.Ball.createEntity(this, pos)
            .setVelocityXY(0, speed);
    }
    if (this.direction.north.canShoot()) {
        pos.x = this.x + this.w / 2;
        pos.y = this.y;
        BG.Ball.createEntity(this, pos)
            .setVelocityXY(0, -speed);
    }
    if (this.direction.east.canShoot()) {
        pos.x = this.x + this.w;
        pos.y = this.y + this.h / 2;
        BG.Ball.createEntity(this, pos)
            .setVelocityXY(speed, 0);
    }
    if (this.direction.west.canShoot()) {
        pos.x = this.x;
        pos.y = this.y + this.h / 2;
        BG.Ball.createEntity(this, pos)
            .setVelocityXY(-speed, 0);
    }
    this.markedToShoot = false;
};
/** @type {Crafty.math.Vector2D} */
BG.Turret.prototype.shoot.tmp_vec2 = new Crafty.math.Vector2D();

/**
 * @return {void}
 */
BG.Turret.prototype.rotateTurretCW = function() {
    var north = this.direction.north;
    this.direction.north = this.direction.west;
    this.direction.west = this.direction.south;
    this.direction.south = this.direction.east;
    this.direction.east = north;

    for (var dir in this.direction) {
        if (this.direction[dir].hasGun()) {
            var gun = this.direction[dir].getGun();
            gun.rotation = (gun.rotation + BG.Turret.ROTATION_AMOUNT) % BG.Turret.FULL_ROTATION;
        }
    }
};

/**
 * @return {void}
 */
BG.Turret.prototype.rotateTurretCCW = function() {
    var north = this.direction.north;
    this.direction.north = this.direction.east;
    this.direction.east = this.direction.south;
    this.direction.south = this.direction.west;
    this.direction.west = north;

    for (var dir in this.direction) {
        if (this.direction[dir].hasGun()) {
            var gun = this.direction[dir].getGun();
            gun.rotation = (gun.rotation - BG.Turret.ROTATION_AMOUNT) % BG.Turret.FULL_ROTATION;
        }
    }
};

/**
 * @return {void}
 */
BG.Turret.prototype.enableAllSides = function() {
    for (var i in this.direction) {
        this.direction[i].enable();
    }
};
/**
 *
 * @constructor
 */
BG.Turret.Direction = function()
{
    /** @type {Crafty.entity} a reference to the actual gun */
    this.gun;
    /** @type {boolean} */
    this.shoot = false;
    /** @type {boolean} */
    this.hasGunx = false;
};

/**
 *
 * @param {Crafty.entity} gun
 */
BG.Turret.Direction.prototype.setGun = function(gun)
{
    this.gun = gun;
};

/**
 *
 * @return {Crafty.entity}
 */
BG.Turret.Direction.prototype.getGun = function()
{
    return this.gun;
};

/**
 *
 * @param {boolean} hasGun
 */
BG.Turret.Direction.prototype.setHasGun = function(hasGun)
{
    this.hasGunx = hasGun;
};

/**
 *
 * @return {boolean}
 */
BG.Turret.Direction.prototype.hasGun = function()
{
    return this.hasGunx;
};

/**
 * @return {void}
 */
BG.Turret.Direction.prototype.enable = function()
{
    this.shoot = true;
};

/**
 * @return {void}
 */
BG.Turret.Direction.prototype.disable = function()
{
    this.shoot = false;
};

/**
 *
 * @return {boolean}
 */
BG.Turret.Direction.prototype.canShoot = function()
{
    return this.shoot && this.hasGunx;
};
