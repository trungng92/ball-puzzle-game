goog.provide('BG.Ball');

/**
 *
 * @constructor
 */
BG.Ball = function() {
    /** @type {Crafty.math.Vector2D} */
    this.velocity = null;

    /** @type {Crafty.entity} */
    this.spawnedFrom = null;
};

/**
 * @return {void}
 */
BG.Ball.prototype.init = function() {
    this.velocity = new Crafty.math.Vector2D();
};

/**
 * @return {void}
 */
BG.Ball.registerComponent = function() {
    Crafty.c('BGBall', new BG.Ball());
};

/**
 * @return {Crafty.entity}
 * @param spawner
 * @param {Crafty.math.Vector2D} pos
 */
BG.Ball.createEntity = function(spawner, pos) {
    BG.Ball.createEntity.tileCounter++;
    var that = Crafty.e('TileObject, BGBall, 2D, Canvas, ' +
        'Collision, BallSprite');

    that.attr({ x: pos.x - 8,
            y: pos.y - 8,
            w: 16,
            h: 16})
        .collision()
        .onHit('BGBall', that.onHitBall)
        .setName('Ball ' + BG.Ball.createEntity.tileCounter)
        .bind('EnterFrame', that.moveBall);
    that.setSpawnedFrom(spawner);
    return that;
};
/** @type {number} */
BG.Ball.createEntity.tileCounter = 0;

/**
 * @return {void}
 */
BG.Ball.prototype.moveBall = function() {
    this.shift(this.velocity.x, this.velocity.y, 0, 0);
};

/**
 * @return {void}
 * @param {Array.<Crafty.entity>} balls
 */
BG.Ball.prototype.onHitBall = function(balls) {
    for (var i = 0; i < balls.length; i++) {
        var ball = balls[i].obj;
        this.bind('ExitFrame', function() {ball.destroy(); });
    }
    var that = this;
    this.bind('ExitFrame', function() {that.destroy(); });
};

/**
 * @return {void}
 * @param {number} xVel
 * @param {number} yVel
 */
BG.Ball.prototype.setVelocityXY = function(xVel, yVel) {
    this.velocity.x = xVel;
    this.velocity.y = yVel;
};

/**
 *
 * @return {string}
 */
BG.Ball.prototype.getDirection = function() {
    if (this.velocity.x > 0) {
        return 'east';
    } else if (this.velocity.x < 0) {
        return 'west';
    } else if (this.velocity.y < 0) {
        return 'north';
    } else if (this.velocity.y > 0) {
        return 'south';
    }
    return '';
};

/**
 *
 * @return {string}
 */
BG.Ball.prototype.getOppositeDirection = function() {
    if (this.velocity.x > 0) {
        return 'west';
    } else if (this.velocity.x < 0) {
        return 'east';
    } else if (this.velocity.y < 0) {
        return 'south';
    } else if (this.velocity.y > 0) {
        return 'north';
    }
    return '';
};

/**
 *
 * not sure what type? maybe there should be a ball spawner interface?
 * @param spawner
 */
BG.Ball.prototype.setSpawnedFrom = function(spawner) {
    this.spawnedFrom = spawner;
};

/**
 * @return {void}
 */
BG.Ball.prototype.markForRemoval = function() {
    var that = this;
    this.one('RemoveBalls', function() {
        that.destroy();
    });
};
