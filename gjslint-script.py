#!C:\Python27\python.exe
# EASY-INSTALL-ENTRY-SCRIPT: 'closure-linter==2.3.13','console_scripts','gjslint'
__requires__ = 'closure-linter==2.3.13'
import sys
from pkg_resources import load_entry_point

if __name__ == '__main__':
    sys.exit(
        load_entry_point('closure-linter==2.3.13', 'console_scripts', 'gjslint')()
    )
